""" boxy tree element. 

Extends ObjectifiedElement in order to provide the functionality needed for 
easier openbox menu element configuration.
"""
from lxml.objectify import ObjectifiedElement
from lxml.objectify import ElementMaker as E
from lxml.etree import tostring, tostringlist

#: Dictionary of element information specific to openbox
ob = { 
    'where':['current','last'],
    'to':['current','next','previous','last','north','up','south','down',
          'west','left','east','right'],
    'action':['Execute','ToggleShowDesktop','GoToDesktop','AddDesktop', 
               'RemoveDesktop','Exit','Restart','Reconfigure'],
    'type':['Menu','Link','Item','Separator','Label','Root'],
    'notify':['name','wmclass']
    }

class ElementError(Exception): pass

class Element(ObjectifiedElement):
    """ Element subclass that defines custom functionality.

    Overrides __setattr__ because the base class was overriding the custom 
    property setters.

    TODO: verify needed setters.
    """

    def __setattr__(self, name, value):
        if not self._set(name, value):
            super(Element, self).__setattr__(name, value)

    def __str__(self):
        #return tostring(self, pretty_print=True)
        return tostring(self)

    def __list__(self):
        return tostringlist(self)


    @property
    def ob_desc(self):
        """ Return a simple description of element. Read-only. """
        if self.get('label') is None:
            if self.get('id') is None:
                return self.tag
            else:
                if self.get('id') == 'root-menu':
                    return 'Root Menu'
                return self.get('id')
        else:
            return self.get('label')

    @property
    def ob_type(self):
        """ Return a simple description of the type of element. Read-only. """
        if self.tag == 'item':
            return 'Item'
        elif self.tag == 'menu':
            if self.get('id') == 'root-menu':
                return 'Root'
            elif self.get('label') is None:
                return 'Link'
            else:
                return 'Menu'
        elif self.tag == 'separator':
            if self.get('label') is None:
                return 'Separator'                   
            else:                    
                return 'Label' 
        return None

    @property
    def ob_command(self):
        """ The element command. """
        if hasattr(self, 'action'):
            if hasattr(self.action, 'execute'):
                return self.action.execute.text                
            elif hasattr(self.action, 'command'):
                return self.action.command.text
        return None

    @property
    def ob_action(self):
        """ The element action name. """
        if hasattr(self, 'action'):
            return self.action.get('name')
        return None

    @property
    def ob_id(self):
        """ The element id. """
        return self.get('id')

    @property
    def ob_label(self):
        """ The element label. """
        return self.get('label')

    @property
    def ob_icon(self):
        """ The element icon. """
        return self.get('icon')

    @property
    def ob_notify(self): 
        """ The element startupnotify name. """
        if hasattr(self, 'action'):
            if hasattr(self.action, 'startupnotify'):
                if hasattr(self.action.startupnotify, 'name'):
                    return self.action.startupnotify.name.text
        return None

    @property
    def ob_notify_class(self):
        """ The element startupnotify wmclass. """
        if hasattr(self, 'action'):
            if hasattr(self.action, 'startupnotify'):
                if hasattr(self.action.startupnotify, 'wmclass'):
                    return self.action.startupnotify.wmclass.text
        return None

    @property
    def ob_prompt(self):
        """ The element prompt message. """
        if hasattr(self, 'action'):
            if hasattr(self.action, 'prompt'):
                return self.action.prompt.text
        return None   
 
    @property
    def ob_to(self): 
        """ The element's 'to' child text. """
        if hasattr(self,'action'):
            if hasattr(self.action, 'to'):
                return self.action.to.text
        return None

    @property
    def ob_where(self):
        """ The element's 'where' child text. """
        if hasattr(self,'action'):
            if hasattr(self.action, 'where'):
                return self.action.where.text
        return None

    def _set(self, name, value):
        """ Custom property setter.

        Custom setters were getting overridden by base. If value is None then
        the setter acts as a deleter. Returns True if attr is handled. Can't
        change a text value from inside element so ElementMaker is used to 
        replace text elements instead.        
        """
        if name == 'ob_label':
            if value is None:
                if self.get('label') is not None:
                    del self.attrib['label']
            else:
                self.set('label',value)
            return True

        elif name == 'ob_id':
            if value is None:
                if self.get('id') is not None:
                    del self.attrib['id']
            else:
                self.set('id',value)
            return True

        elif name == 'ob_icon':
            if value is None:
                if self.get('icon') is not None:
                    del self.attrib['icon']
            else:
                self.set('icon',value)
            return True

        elif name == 'ob_action':
            if hasattr(self, 'action'):
                if value is None:
                    self.action.delete()
                elif value in ob['actions']:
                    self.action.set('name', value)
            return True

        elif name == 'ob_prompt':
            if hasattr(self, 'action'):
                if value is None:
                    if hasattr(self.action,'prompt'):
                        self.action.prompt.delete()
                else:
                    self.action.prompt = E(annotate=False).prompt(value)
            return True

        elif name == 'ob_notify':
            if hasattr(self, 'action'):
                if value is None:
                    if hasattr(self.action,'startupnotify'):
                        self.action.startupnotify.delete()
                else:
                    self.action.startupnotify.name = E(annotate=False).name(
                        value)
            return True

        elif name == 'ob_notify_class':   
            if hasattr(self, 'action'):
                if value is None:
                    if hasattr(self.action, 'startupnotify'):
                        if hasattr(self.action.startupnotify,'wmclass'):
                            self.action.startupnotify.wmclass.delete()
                else:
                    self.action.startupnotify.wmclass = E(annotate=False
                                                          ).wmclass(value)
            return True

        elif name == 'ob_where':
            if hasattr(self, 'action'):
                if value is None:
                    if hasattr(self.action, 'where'):
                        self.action.where.delete()
                elif value in ob['where']:
                    self.action.where = E(annotate=False).where(value)
            return True

        elif name == 'ob_to':
            if hasattr(self, 'action'):
                if value is None:
                    if hasattr(self.action, 'to'):
                        self.action.to.delete()
                elif value in ob['to']:
                    self.action.to = E(annotate=False).to(value)
            return True  

        elif name == 'ob_command':
            if hasattr(self, 'action'):
                if hasattr(self.action, 'execute'):
                    if value is None:
                        self.action.execute.delete()
                    else:
                        self.action.execute = E(annotate=False).execute(value)
                elif hasattr(self.action, 'command'):
                    if value is None:
                        self.action.command.delete()
                    else:
                        self.action.command = E(annotate=False).command(value)
                elif value is not None:
                    self.action.command = E(annotate=False).command(value)
            return True

        return False
    

    def search(self, tag, **attribs):
        """ Find and return an element or list of elements with xpath. """
        if attribs is not None:
            a = "".join("[@{0}='{1}']".format(k,v) for k,v in attribs.items())
            s = "{0}{1}".format(tag, a)
            element = self.xpath(s)
            return element[0] if len(element) == 1 else element
        else:
            return None

    def delete(self):
        """ Remove element from its parent. """
        parent = self.getparent()
        parent.remove(self)

    def moveup(self):
        """ Move element up 1 position.

        Return True if element has a previous sibling. 
        """
        previous = self.getprevious()
        if previous is not None:
            self.delete()
            previous.addprevious(self)
            return True
        else:
            return False

    def movedown(self):
        """ Move element down 1 position.

        Return True if element has a next sibling.        
        """
        next = self.getnext()
        if next is not None:
            self.delete()
            next.addnext(self)
            return True
        else:
            return False        
