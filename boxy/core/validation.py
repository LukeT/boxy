""" Element validation. 

Ensures the contents of elements are valid for openbox menus. Validators for
every element, which are accessed by the validators dictionary, all return True
if valid, else False. These should only be called after all elements and 
children are initialized, which discounts iterparse and _init validation.
"""
from element import Element, ob


class ValidationError(Exception): pass


def validateElement(e):
    """ Validate a single element.

    Main entry for all element validation. 
    """
    if isinstance(e, Element):
        if e.tag in validators.keys():
            return validators[e.tag](e)
    return False


def validateTree(root): 
    """ Validate every descendant element of root. 

    Return the first invalid element or None if valid.
    """
    if not validateElement(root):
        return False, root
    for e in root.iterdescendants():
        if not validateElement(e):
            return False, e
    return True, None


""" Validation cases, each one matching a value in the dictionary. """

def validRoot(e): 
    """ Validate an openbox menu root menu. """
    if hasattr(e,'menu'):
        for menu in e.menu:
            if menu.get('id') == 'root-menu':
                return True
    return False

def validMenu(e):
    """ Validate an openbox menu. """
    if e.get('id') is not None:
        if e.get('label') is not None:
            return True #menu
        return True #link
    return False

def validItem(e):
    """ Validate an openbox item. """
    if e.get('label') is not None:
        if hasattr(e, 'action'):
            if len(e.action) == 1:
                return True    
    return False

def validSimple(e):
    """ Validate an openbox text only element. """
    if e.countchildren() == 0:#no children
        if e.keys() == []: #no attributes
            if hasattr(e, 'text'): #has text
                return True
    return False

def validTo(e): 
    """ Validate an openbox to. """
    return validSimple(e) and e.text in ob['to']

def validWhere(e): 
    """ Validate an openbox where. """
    return validSimple(e) and e.text in ob['where']

def validBool(e):
    """ Validate an openbox boolean. """
    return validSimple(e) and e.text in ['yes','no']

def validSep(e):
    """ Validate an openbox separator. """
    if e.countchildren() == 0:#no children
        if e.keys() == ['label'] or e.keys() == []:
            return True
    return False

def validNotify(e): 
    """ Validate an openbox startupnotify. """
    if hasattr(e, 'enabled'):
        if not validBool(e.enabled):
            return False
    if hasattr(e, 'name'):
        if not validSimple(e.name):
            return False
    if hasattr(e, 'wmclass'):
        if not validSimple(e.wmclass):
            return False
    return True

def validAction(e): 
    """ Validate an openbox action. """
    if e.keys() == ['name']:
        action = e.get('name')
        if action == 'Execute': # has valid command or execute
            if hasattr(e, 'command'):
                if not validSimple(e.command):
                    return False
            elif hasattr(e, 'execute'):
                if not validSimple(e.execute):
                    return False
            else:
                return False
        #names that depend on nothing
        elif action == 'Exit': pass
        elif action == 'Restart': pass
        elif action == 'Reconfigure': pass
        elif action == 'ToggleShowDesktop': pass
        elif action == 'GoToDesktop':
            if hasattr(e, 'to'):
                if not validTo(e.to):
                    return False
            else:
                return False
        elif action == 'AddDesktop' or action == 'RemoveDesktop':
            if hasattr(e, 'where'):
                if not validWhere(e.where):
                    return False
            else:
                return False
        else:
            return False
    else:
        return False

    return True


""": Dictionary of validator functions. """
validators = {
    'openbox_menu':validRoot, 'menu':validMenu, 'item':validItem,
    'action':validAction, 'execute':validSimple, 'wmclass':validSimple, 
    'startupnotify':validNotify, 'to':validTo, 'where':validWhere,
    'enabled':validBool, 'separator':validSep, 'prompt':validSimple,
    'icon':validSimple, 'wrap':validBool, 'name':validSimple, 
    'command':validSimple
    }
