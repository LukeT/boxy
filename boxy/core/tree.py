""" tree module. 

Simplifies the manipulation of openbox menus with lxml.objectify.
"""
import sys
import os
from subprocess import check_call
from lxml import etree
from lxml import objectify
from errno import ENOENT as notfound


from boxy.core.element import Element, ElementError
from boxy.core.validation import validateTree, ValidationError

class TreeError(Exception): pass

class Tree(object):
    """  Main class for manipulating openbox xml menus. """

    _tree = None
    _root = None
    _filename = None
    _parser = None
    _elementFactory = None
    _ob_dir = None

    @property
    def E(self):
        """ The element factory for the tree. """
        return self._elementFactory
    @property
    def parser(self):
        """ The element parser for the tree. """
        return self._parser
    @property
    def filename(self):
        """ The filename of the tree. """
        return self._filename
    @property
    def root(self):
        """ The root node of the tree. """
        return self._root
    @property
    def tree(self):
        """ The lxml tree. """
        return self._tree
    @property
    def ob_dir(self):
        """ The openbox directory. """
        return self._ob_dir
    @property
    def isvalid(self):
        """ Return (True,None) if tree is valid, else (False,Element). """ 
        return validateTree(self.root)

    def __init__(self, filename=None):
        #schema = etree.DTD(schemaFile)
        #self._schema = etree.XMLSchema(schema)
        lookup = etree.ElementDefaultClassLookup(element=Element)      
        #self._parser = etree.XMLParser(schema=self.schema)
        self._ob_dir = os.path.expanduser('~') + '/.config/openbox/'
        self._parser = etree.XMLParser()
        self._parser.set_element_class_lookup(lookup)
        self._elementFactory = objectify.ElementMaker(
            annotate=False,
            makeelement=self.parser.makeelement)        
        if filename is None:
            self.load('{0}menu.xml'.format(self.ob_dir))
        else:
            self.load(filename)
    
    def load(self, filename):
        """ Attempt to load the received file. Absolute path required. """
        f = os.path.abspath(filename)
        if os.path.isfile(f):
            tree = objectify.parse(f, parser=self.parser)
            isvalid, e = validateTree(tree.getroot())
            if isvalid:
                self._tree = tree
                self._root = tree.getroot()
                self._filename = f
            else:
                raise ValidationError("invalid menu file.")
        else:
            raise IOError(notfound,"xml file not found:",f)

    def new(self, filename=None):
        """ Create a barebones xml menu with objectify's element factory. """
        self._root = self._defaultRoot()
        self._tree = self.root.getroottree() 
        self._filename = filename

    def save(self, filename=None):
        """ Save the current tree to file. """
        if filename is None:
            if self.filename is None:
                raise TreeError("no filename provided.")
            else:
                filename = self.filename        
        self._filename = filename
        etree.ElementTree(self.root).write(filename, 
                                           xml_declaration=True,
                                           encoding='utf-8',
                                           pretty_print=True)
       
    def install(self, restore_backup=False):
        """ Install as menu.xml or restore the backup and refresh openbox.

        If restore_backup is True then menu.BACKUP and menu.xml will be swapped,
        otherwise the current menu.xml will be renamed menu.BACKUP and the
        current tree will be saved as menu.xml.        
        """        
        if not os.path.isdir(self.ob_dir):
            raise IOError(notfound,"directory not found:",self.ob_dir)
        else:
            menu = self.ob_dir + 'menu.xml'
            backup = self.ob_dir + 'menu.BACKUP'
            temp = self.ob_dir + 'temp'
            if not restore_backup:
                if os.path.isfile(menu):
                    os.rename(menu, backup)
                    self.save(menu)
                else:                      
                    self.save(menu) 
                check_call(["openbox", "--restart"])
            else:
                if os.path.isfile(backup):
                    os.rename(menu, temp)
                    os.rename(backup, menu)
                    os.rename(temp, backup)
                    self.load(menu)
                    check_call(["openbox", "--restart"])
                else:
                    raise IOError(notfound,"backup file not found:",backup)

    def traverse(self, callback, element=None, **kwargs):
        """ Recursively traverse the tree and invoke the callback.

        The callback must take (element, **kwargs) as its parameters as well as 
        return (element, kwargs), and can therefore read and write to the kwargs
        dictionary and element.
        """
        if element is None: element = self.root
        element, kwargs = callback(element, **kwargs)
        for child in element.getchildren():
            self.traverse(callback, child, **kwargs)
        
    def __str__(self):
        return etree.tostring(self.root, xml_declaration=True, encoding="utf-8",
                              pretty_print=True)
    
    def _defaultRoot(self):
        """ Create and return a sane, hardcoded, default root element. """
        E = self.E
        return E.openbox_menu(
            E.menu(
                E.item(E.action(name="Reconfigure"), label="reconfigure"),
                E.item(E.action(name="Restart"), label="restart"),
                E.item(E.action(E.prompt("Really exit openbox?"), name="Exit"),
                       label="exit"),
                label='openbox', id='openbox-menu'),
            E.menu(
                E.item(E.action(name="ToggleShowDesktop"), label="show/hide"),
                E.item(E.action(E.to("last"), E.wrap("yes"), name="GoToDesktop"),
                       label="goto last"),
                E.item(E.action(E.where("last"),name="AddDesktop"),
                       label="add last"),
                E.item(E.action(E.where("last"),E.prompt("remove last desktop?"),
                                name="RemoveDesktop"),
                       label="remove last"),
                label='desktop',id='desktop-menu'),
            E.menu(
                E.item(E.action(E.startupnotify(E.enabled("yes"),
                                                E.name("firefox"),
                                                E.wmclass("firefox")),
                                E.command("firefox"),
                                name="Execute"),
                       label="firefox"),
                E.item(E.action(E.command("xterm"), name="Execute"),
                       label="xterm"),
                E.item(E.action(E.command("xterm -s python"), name="Execute"),
                       label="python"),
                E.item(E.action(E.command("xterm -s top"), name="Execute"), 
                       label="top"),
                E.separator(),
                E.menu(id='client-list-menu'),     
                E.separator(label="menus..."),
                E.menu(id='desktop-menu'),
                E.menu(id='openbox-menu'),
                label='root menu', id='root-menu')
            )

    def element(self, tag='item', **kwargs):
        """ Creates an element from kwargs.

        Returns the constructed element if it is valid. Uses the element factory
        to create elements. Can only create elements with tags: item, menu and
        separator.        
        """
        valid = ['item','menu','separator']
        if tag in valid:
            element = None
            E = self.E
            keys = kwargs.keys()
            attr = {}            
            if tag == 'menu':
                if 'id' in keys:
                    attr['id'] = kwargs['id']
                if 'label' in keys:
                    attr['label'] = kwargs['label']
                if 'icon' in keys:
                    attr['icon'] = kwargs['icon']
                element = E.menu(**attr)
            elif tag == 'separator':
                if 'label' in keys:
                    attr['label'] = kwargs['label']
                element = E.separator(**attr)                
            elif tag == 'item':                
                if 'label' in keys:
                    attr['label'] = kwargs['label']
                if 'icon' in keys:
                    attr['icon'] = kwargs['icon']
                if 'action' in keys: 
                    action = E.action(name=kwargs['action'])
                    if 'command' in keys: 
                        action.append(E.command(kwargs['command']))
                    if 'prompt' in keys: 
                        action.append(E.prompt(kwargs['prompt']))        
                    if 'to' in keys:
                        action.append(E.to(kwargs['to']))
                    if 'where' in keys:
                        action.append(E.where(kwargs['where']))
                    if 'wrap' in keys:
                        action.append(E.wrap(kwargs['wrap']))
                    if 'notify'in keys:
                        if 'notify_class' in keys: 
                            wmclass = kwargs['notify_class']
                        else: 
                            wmclass = kwargs['notify']
                        action.append(E.startupnotify(E.enabled("yes"), 
                                                      E.name(kwargs['notify']),
                                                      E.wmclass(wmclass)))
                    element = E.item(action, **attr)
            isvalid, e = validateTree(element)
            if isvalid:
                return element
            else:
                raise ValidationError("{0} element is invalid.".format(e.tag))
        else:
            raise ElementError("invalid tag. must be: item, menu or separator.")
