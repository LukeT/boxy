""" Main gtk user interface module. """

import gtk
import sys
import os
from errno import ENOENT as notfound

from boxy.core.tree import Tree
from boxy.gtk2.handler import Handler
from boxy.gtk2.meta import Meta
from boxy.gtk2 import dialogs

GLADEFILE = "boxy/gtk2/boxy.glade"


class MainWindow(object):
    """ Main gtk window container.

    TODO: add a toplevel SaveAs() (redundancy in meta and handler).
    """
    #__metaclass__ = Meta

    tree = None
    builder = None
    treeview = None
    treestore = None 
    output = None
    window = None 
    handler = None

    def __init__(self):
        if os.path.isfile(GLADEFILE):            
            self.tree = Tree()
            self.builder = gtk.Builder()
            self.builder.add_from_file(GLADEFILE)
            self.output = self.builder.get_object("statusbar")
            self.treeview_setup()       
            self.window = self.builder.get_object("window")
            self.toplevel = self.window            
            self.handler = Handler(self)
            self.builder.connect_signals(self.handler)
            self.treeview_populate()
            self.window.connect("delete-event", self.handler.clicked_quit)
            self.window.show_all()
        else:
            raise IOError(notfound,"glade file not found:",GLADEFILE)

    def prompt(self, s):
        """ Open the prompt dialog and ask a question. """
        dialog = dialogs.Prompt(self.window, s)
        response = dialog.run()
        dialog.destroy()
        return response

    def stderr(self, s): 
        """ Open the error dialog and show the message. """
        dialog = dialogs.Error(self.window, s)
        dialog.run()
        dialog.destroy()
    
    def title(self, s):
        """ Set the window title. """
        self.window.set_title('{0} - boxy'.format(s))

    def stdout(self, s):
        """ Push the message to the statusbar. """
        self.output.push(0, s)

    def quit(self, *args, **kwargs):
        """ Quit gtk. """
        gtk.main_quit()

    def treeview_setup(self):
        """ Set up the treeview. """        
        self.treestore = gtk.TreeStore(str, str, str, object)
        self.treeview = self.builder.get_object("treeview")
        self.treeview.set_model(self.treestore)
        renderer = gtk.CellRendererText()
        col = gtk.TreeViewColumn('Element', renderer, text=0)
        self.treeview.append_column(col)
        col = gtk.TreeViewColumn('Type', renderer, text=1)
        self.treeview.append_column(col)
        col = gtk.TreeViewColumn('Action', renderer, text=2)
        self.treeview.append_column(col)
   
    def treeview_populate(self):
        """ Populate the treeview with the element data from Tree.

        Calls tree's traversal function to touch all of root's elements. Only 
        populates with elements that return an ob_type.         
        """
        def populate_row(element, **kwargs):
            """ Add a row to the treeview. """   
            if element.ob_type is not None:
                node = [ element.ob_desc, element.ob_type, element.ob_action,
                         element ] 
                previous = kwargs['parent']
                kwargs['parent'] = self.treestore.append(kwargs['parent'], node)
            return element, kwargs
        self.tree.traverse(populate_row, parent=None)
        self.treeview.expand_all()        
        self.title(self.tree.filename or "*unsaved")

    def treeview_refresh(self):
        """ Clear and re-populate the treeview. """
        self.treestore.clear()
        self.treeview_populate()

 

def main():
    """ GTK start point. """
    window = MainWindow()
    gtk.main()
    
