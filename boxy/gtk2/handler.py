""" Gtk2 signal handlers for everything in boxy.glade. """

from boxy.core.tree import Tree
from boxy.gtk2.meta import Meta
from boxy.gtk2 import dialogs
import gtk
import os


class Handler(object):
    """ GTK signal handler class. 

    Function names must match gladefile signal handler names. Tracks saved state
    by prefixing the title with an asterisk when the tree is changed. Shares
    3 common attributes with MainWindow in order to share Meta: stderr, tree, 
    and toplevel... everything else can be accessed through self.main.

    TODO: handle cut, copy, and paste.
    """ 
    #__metaclass__ = Meta

    def __init__(self, main): 
        self.main = main
        self.toplevel = self.main.window
        self.stderr = self.main.stderr
        self.tree = self.main.tree
        self.selection = None
        self.ElementDialog = dialogs.Element(main.builder)

    def changed_row(self, obj):
        """ Set current element to the new selection. """
        self.selection = Selection(obj)

    def clicked_about(self, obj, data=None):
        """ Handle the about dialog. """
        dialog = dialogs.About()
        dialog.run()
        dialog.destroy()
        
    def clicked_new(self, obj, data=None):
        """ Handle creating a new tree. """
        self.tree.new()
        self.main.treeview_refresh() 
        self.main.stdout("Loaded example menu.")
        self.main.title('*unsaved menu')

    def clicked_open(self, obj, data=None):
        """ Handle loading a file from disk. """
        dialog = dialogs.Open(self.toplevel)
        response = dialog.run()  
        filename = dialog.get_filename()         
        dialog.destroy()
        if response == gtk.RESPONSE_OK:            
            if filename is not None:
                self.tree.load(filename)
                self.main.treeview_refresh() 
                self.main.stdout("Loaded: {0}.".format(filename))

    def clicked_save(self, obj, data=None):
        """ Handle saving the tree to disk. """
        self.tree.save()
        self.main.title(self.main.tree.filename)

    def clicked_saveas(self, obj, data=None):
        """ Handle saving the tree to disk with a new filename. """
        dialog = SaveAs(self.toplevel)
        response = dialog.run()        
        filename = dialog.get_filename()
        dialog.destroy()
        if response == gtk.RESPONSE_OK:
            self.tree.save(filename)
            self.main.title(filename)

    def clicked_install(self, obj, data=None):
        """ Handle installing menu to .config/openbox and backup. """
        prompt = ("Are you sure you wish to install the current menu?\n"
                  "The current menu will be saved as:\n\n"
                  "~/.config/openbox/menu.xml")
        response = self.main.prompt(prompt)
        if response == gtk.RESPONSE_YES:
            self.main.tree.install()
            self.stdout("Installed the current menu.")
    
    def clicked_restore(self, obj, data=None):
        """ Handle restoring the menu backup. """
        self.main.tree.install(restore_backup=True)
        self.main.treeview_refresh()
        self.stdout("Restored and loaded the backup menu.")

    def clicked_quit(self, obj, data=None):
        """ Handle quitting the application. """
        self.main.quit()

    def clicked_remove(self, obj, data=None):
        """ Handle removing an element from the tree. """
        if self.selection is not None:
            if self.selection.element.ob_type != 'Root':
                self.selection.element.delete()
                self.treestore.remove(self.selection.current)
                self.main.title('*{0}'.format(self.main.tree.filename))
            else:
                self.stderr("The root menu is required.\nRemoval denied.")
        else:
            self.stderr("Nothing selected.")

    def clicked_moveup(self, obj, data=None):
        """ Handle moving an element up the tree. """
        if self.selection is not None:
            if self.selection.element.moveup():
                self.main.treestore.swap(self.selection.current,
                                         self.selection.prev)
                selection = self.main.treeview.get_selection()
                selection.select_iter(self.selection.current)
                self.selection = Selection(self.main.treeview)
                self.main.title('*{0}'.format(self.main.tree.filename))
            else:
                self.stderr("Cannot move element up any further.")
        else:
            self.stderr("Nothing selected.")

    def clicked_movedown(self, obj, data=None):
        """ Handle moving an element down the tree. """
        if self.selection is not None:
            if self.selection.element.movedown():
                self.main.treestore.swap(self.selection.current, 
                                         self.selection.next)
                selection = self.main.treeview.get_selection()
                selection.select_iter(self.selection.current)
                self.selection = Selection(self.main.treeview)
                self.main.title('*{0}'.format(self.main.tree.filename))
            else:
                self.stderr("Cannot move element down any further.")
        else:
            self.stderr("Nothing selected.")
 
    def clicked_add(self, obj, data=None):
        """ Handle adding an element with the dialog. """
        self.ElementDialog.run()
        self.ElementDialog.hide()
        self.main.title('*{0}'.format(self.main.tree.filename))

    def clicked_row(self, obj, data, *args, **kwargs):
        """ Handle clicking on a row in the treeview.
        
        Populates and runs ElementDialog with the current selection.element.
        """
        self.ElementDialog.populate(self.selection.element)
        self.ElementDialog.run()
        self.ElementDialog.hide()



    """ Element Dialog Handlers """



    def clicked_element_ok(self, obj, data=None):
        """ Handle the ok button. """
        
        selection = self.selection.element
        tag, kw = self.ElementDialog.get_element()
        e = self.tree.element(tag, **kw)
        selstring = ''
        for s in str(selection).splitlines():
            selstring += s.strip()

        print selstring
        print e

        changes = selstring.strip() != str(e).strip()
        if changes: print 'changed'

        #for i in list(self.selection.element): print i
        #for s in list(self.selection.element): print s.strip()

    def clicked_element_cancel(self, obj, data=None):
        """ Handle the Cancel Button. """
    
    def clicked_element_type(self, obj, data=None):
        """  """
    def clicked_element_label(self, obj, data=None):
        """  """
    def clicked_element_action(self, obj, data=None):
        """  """
    def clicked_element_id(self, obj, data=None):
        """  """
    def clicked_element_command(self, obj, data=None):
        """  """
    def clicked_element_to(self, obj, data=None):
        """  """
    def clicked_element_where(self, obj, data=None):
        """  """
        
       




class Selection(object):
    """ Containment class for gtk tree row. 

    Implemented because gtk2 treestore has no iter_previous(), otherwise this
    class would be unnessesary.
    """
    _prev = None
    _next = None
    _current = None
    _element = None
    @property
    def prev(self):
        """ Returns the treeiter of the previous sibling. """
        return self._prev
    @property
    def next(self): 
        """ Returns the treeiter of the next sibling. """
        return self._next
    @property
    def current(self): 
        """ Returns the treeiter of the current selection. """
        return self._current
    @property
    def element(self): 
        """ Returns the element of the current selection. """
        return self._element

    def __init__(self, treeview):
        selection = treeview.get_selection()
        (model, i) = selection.get_selected()
        prev = None
        next = model.iter_next(i)
        path = model.get_path(i)
        row = path[0]
        last = path[-1]
        depth = len(path)
        if last > 0:
            if depth == 1:
                if row > 0:
                    prev = model[(row-1)]
            elif depth == 2:
                prev = model[(row,last-1)]
            elif depth > 2:
                t = path[:-1] + (last-1,)
                prev = model[t]
        self._prev = None if prev is None else prev.iter            
        self._next = None if next is None else next
        self._current = i
        self._element = model.get_value(i, 3)
