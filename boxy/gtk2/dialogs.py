# -*- coding: utf-8 -*-
""" gtk.Dialogs called by MainWindow.

Explicit encoding for copyright symbol in About dialog.
"""

import gtk
from boxy.core.element import ob

LICENSE = """\
boxy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

boxy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with boxy.  If not, see <http://www.gnu.org/licenses/>.
"""

def file_filter():
    """ File filter for FileChooserDialogs. """
    xml_filter = gtk.FileFilter()
    xml_filter.set_name("XML files")
    xml_filter.add_mime_type("text/xml")
    xml_filter.add_pattern("*.xml")
    return xml_filter


class SaveAs(gtk.FileChooserDialog):
    """ Dialog for saving new files. """
    def __init__(self, parent):
        super(SaveAs, self).__init__("Save as...", parent,
                                     gtk.FILE_CHOOSER_ACTION_SAVE,
                                     (gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
                                      gtk.STOCK_OK, gtk.RESPONSE_OK))        
        self.add_filter(file_filter())
        self.set_do_overwrite_confirmation(True)
        self.show_all()
        

class Open(gtk.FileChooserDialog):
    """ Dialog for opening files. """
    def __init__(self, parent):
        super(Open, self).__init__("Open...", parent,
                                   gtk.FILE_CHOOSER_ACTION_OPEN,
                                   (gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
                                    gtk.STOCK_OK, gtk.RESPONSE_OK))  
        self.add_filter(file_filter())
        self.show_all()


class Save(gtk.MessageDialog):
    """ Dialog for saving. Custom cancel/no/yes. """
    def __init__(self, parent, filename):
        super(Save, self).__init__(parent, 0, gtk.MESSAGE_QUESTION)
        cancel = gtk.Button(stock=gtk.STOCK_CANCEL)
        self.add_action_widget(cancel, gtk.RESPONSE_CANCEL)
        no = gtk.Button(stock=gtk.STOCK_NO)
        self.add_action_widget(no, gtk.RESPONSE_NO)
        yes = gtk.Button(stock=gtk.STOCK_YES)
        self.add_action_widget(yes, gtk.RESPONSE_YES)
        self.set_markup("Save changes to '{0}'?".format(filename.split('/')[-1]))
        self.set_title("Changes have been made")
        self.show_all()
        

class Prompt(gtk.MessageDialog):
    """ Dialog for generic prompt warnings """
    def __init__(self, parent, message=None): 
        super(Prompt, self).__init__(parent, 0, gtk.MESSAGE_WARNING,
                                     gtk.BUTTONS_YES_NO, message)
        self.set_title("Prompt")
        self.show_all()


class Error(gtk.MessageDialog):
    """ Dialog for error output. """
    def __init__(self, parent, message=None): 
        super(Error, self).__init__(parent, 0, gtk.MESSAGE_ERROR, gtk.BUTTONS_OK,
                                    message)
        self.set_title("Error")
        self.show_all()


class About(gtk.AboutDialog):
    """ Dialog for information about boxy. """
    def __init__(self):
        super(About, self).__init__()
        self.set_name("boxy")
        self.set_version("1.0")
        self.set_comments("An easy to use menu \neditor for openbox.")
        self.set_website("http://www.github.com/luketice/boxy")
        self.set_website_label("boxy @ github")
        self.set_copyright('Copyright © 2013 Luke Tice')        
        self.set_license(LICENSE)


class Element(object):
    """ Element dialog container.

    TODO: make the root type option unselectable.
    TODO: integrate item creation already, then update version.
    """
    HBOXES = ['action','id','label','command','to','where','prompt','icon',
              'notify']
    COMBOS = ['where','to','type','action']

    dialog = None
    combo = {}
    data = {}
    hbox = {}
    check = {}

    def __init__(self, builder):
        """ Initializes dialog, combobox, hbox, and data widgets. """
        self.dialog = builder.get_object("dialog_element")
        for combo in self.COMBOS:
            obj = builder.get_object("combo_{0}".format(combo))
            store = gtk.ListStore(str)
            obj.set_model(store)
            cell = gtk.CellRendererText()
            obj.pack_start(cell, True)
            obj.add_attribute(cell, 'text', 0)
            iterators = {}
            for option in ob[combo]:
                iterators[option] = store.append([option])
                if option == 'Root': pass
            self.combo[combo] = obj, iterators      
        for hbox in self.HBOXES:
            self.hbox[hbox] = builder.get_object('hbox_{0}'.format(hbox))
        self.data['desc'] = builder.get_object("label_element")
        self.data['id'] = builder.get_object("entry_id")
        self.data['prompt'] = builder.get_object("entry_prompt")
        self.data['icon'] = builder.get_object("file_icon")
        self.data['command'] = builder.get_object("entry_command")
        self.data['label'] = builder.get_object("entry_label")
        self.data['notify'] = builder.get_object("entry_notify")
        self.data['notify_class'] = builder.get_object("entry_notify_class")
        self.check['notify'] = builder.get_object("check_notify")
        self.check['prompt'] = builder.get_object("check_prompt")
        self.check['icon'] = builder.get_object("check_icon")

    def run(self):
        """ Run the dialog. """
        self.dialog.run()

    def hide(self):
        """ Hide the dialog. """
        self.dialog.hide()
        self.reset()

    def reset(self):
        """ Reset data, comboboxes, checkboxes, and hboxes to default values. """
        for widget in self.data.values():
            if isinstance(widget, gtk.Entry):
                widget.set_text("")
        [hbox.set_visible(False) for hbox in self.hbox.values()]               
        [combo.set_active(-1) for combo, i in self.combo.values()]
        [check.set_active(False) for check in self.check.values()]
        self.data['notify'].set_sensitive(False)
        self.data['notify_class'].set_sensitive(False)
        self.data['prompt'].set_sensitive(False)
        self.data['icon'].set_sensitive(False)
    
    
    """ boxy element interaction... """


    def populate(self, element):
        """ Populate the widgets with element data. 

        Makes visible widgets that are required. Populates relevant fields
        with the custom element properties (element.ob_*).
        """
        ob_type = element.ob_type
        actions_combo, actions_iters = self.combo['action'] 
        where_combo, where_iters = self.combo['where']
        to_combo, to_iters = self.combo['to']
        types_combo, types_iters = self.combo['type']
        types_combo.set_active_iter(types_iters[ob_type]) 
        #populate universal data
        self.data['desc'].set_text(element.ob_desc)
        if element.get('id') is not None:
            self.data['id'].set_text(element.get('id'))
        if element.get('label') is not None:
            self.data['label'].set_text(element.get('label'))
        #parse and populate based on ob_type    
        if ob_type == 'Item':
            #populate universal item data
            ob_action = element.ob_action  
            self.hbox['action'].set_visible(True)
            self.hbox['label'].set_visible(True)
            self.hbox['icon'].set_visible(True)          
            actions_combo.set_active_iter(actions_iters[ob_action])
            if element.ob_prompt is not None:
                self.check['prompt'].set_active(True)
                self.data['prompt'].set_text(element.ob_prompt)
                self.data['prompt'].set_sensitive(True)
            self.hbox['prompt'].set_visible(True)
            if element.ob_icon is not None:
                self.check['icon'].set_active(True)
                self.data['icon'].set_sensitive(True)
                #self.data['icon'].set_text(element.ob_icon)
            self.hbox['icon'].set_visible(True)
            #parse and populate item based on ob_action
            if ob_action == 'Execute':              
                if element.ob_notify is not None:
                    self.check['notify'].set_active(True)
                    self.data['notify'].set_text(element.ob_notify)
                    self.data['notify_class'].set_text(element.ob_notify_class
                                                       or "")
                    self.data['notify_class'].set_sensitive(True)
                    self.data['notify'].set_sensitive(True)
                self.hbox['notify'].set_visible(True)                
                self.data['command'].set_text(element.ob_command or "")
                self.hbox['command'].set_visible(True)
            elif ob_action == 'AddDesktop' or ob_action == 'RemoveDesktop':
                where_combo.set_active_iter(where_iters[element.ob_where])
                self.hbox['where'].set_visible(True)
            elif ob_action == 'GoToDesktop':
                to_combo.set_active_iter(to_iters[element.ob_to])
                self.hbox['to'].set_visible(True)
        elif ob_type == 'Menu': 
            self.hbox['label'].set_visible(True)
            self.hbox['id'].set_visible(True)
        elif ob_type == 'Link': 
            self.hbox['id'].set_visible(True)
        elif ob_type == 'Label':
            self.hbox['label'].set_visible(True)
    

    def get_element(self):
        """ Returns the updated dialog data. """
        kwargs = {}
        tag = ''

        for combo in self.combo:
            obj, i = self.combo[combo]
            text = obj.get_active_text()
            if combo == 'type':  
                if text in ['Link','Root','Menu']:
                    tag = 'menu'
                elif text == 'Label':
                    tag = 'separator'
                else:
                    tag = text.lower()                
            else: 
                if text is not None:
                    kwargs[combo] = text
        for i in self.data:
            if i == 'icon': break
            text = self.data[i].get_text()
            if text != '':
                kwargs[i] = text  
    
        return tag, kwargs
