""" Metaclasses.

For wrapping multiple class methods in common functionality. 
"""
from subprocess import CalledProcessError
from lxml.etree import XMLSyntaxError
from boxy.core.validation import ValidationError
from boxy.gtk2.dialogs import Save, SaveAs

import gtk
import sys

class Meta(type):
    """ Metaclass for gtk gui classes. 

    Allows wrapping class methods with exception handling and saved state
    checking. Everything prefixed with self must be implemented by the class.
    Augments instance creation.
    """

    def __new__(cls, name, bases, attrs):
        """ Wrap callable attributes on class creation. 

        Functions to exclude from try_except() are noted in the excludes list. 
        Functions to include with prompt_save() are in the includes list.
        """
        includes = ['quit','clicked_restore','clicked_open','clicked_new']
        excludes = ['stderr','__metaclass__','__doc__','__module__']
        for key in attrs.keys(): 
            if hasattr(attrs[key], '__call__'): 
                if key not in excludes:
                    if key in includes:
                        attrs[key] = cls.prompt_save(attrs[key])
                    attrs[key] = cls.try_except(attrs[key])                    
        return type(name, bases, attrs)

    @staticmethod
    def try_except(function):
        """ Call the function and catch exceptions.

        Wraps functions with a try/except and prints exceptions using stderr.
        Pango formatting in the Error dialog is sketchy, so some messages are
        markup, while others are not.
        """
        def wrap(self, *obj, **data):
            try: 
                response = function(self, *obj, **data)
            except CalledProcessError as e:
                cmd = " ".join(e.cmd)
                self.stderr('<b>Subprocess failed:</b>\n\n{0}'.format(cmd))
            except XMLSyntaxError as e:
                self.stderr('<b>Parse failed:</b>\n\nFile is not valid xml.')
            except IOError as e:
                self.stderr('<b>{0}</b>\n\n{1}'.format(e.strerror, e.filename))
            except ValidationError as e:
                self.stderr('Validation Failed:\n\nFile is not a valid menu.')
            except Exception as e:
                self.stderr('{0}'.format(e))
            except: 
                self.stderr('{0}'.format(sys.exc_info[1]))
            else:
                return response
        return wrap

    @staticmethod
    def prompt_save(function):
        """ Save the tree and call the function. 

        Wraps functions that change the state of the current tree with a
        Save/SaveAs dialog. Checks if the tree has been changed and prompts to
        save it if it has.    
        """
        def wrap(self, *obj, **data):
            if self.toplevel.get_title()[0] == '*':
                if self.tree.filename is None:
                    filename = None
                else:
                    filename = self.tree.filename.split('/')[-1]
                dialog = Save(self.toplevel, filename or 'unnamed')
                response = dialog.run()
                dialog.destroy()
                if response == gtk.RESPONSE_YES: 
                    if filename is None:
                        dialog = SaveAs(self.toplevel)
                        response = dialog.run()
                        filename = dialog.get_filename()
                        dialog.destroy()
                        if response == gtk.RESPONSE_OK:
                            self.tree.save(filename)
                    else:
                        self.tree.save()
                    function(self, *obj, **data)
                elif response == gtk.RESPONSE_NO:                    
                    function(self, *obj, **data)
            else:
                function(self, *obj, **data)
        return wrap 


