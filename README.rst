boxy is an openbox menu manager. It was built as a replacement to obmenu with python and lxml.

Designed as an easy to use GUI on top of an equally easy to use API.

Getting Started
---------------

Download::

    	git clone https://github.com/luketice/boxy.git

Install::
	
    	cd boxy
	python setup.py install

Run::
	
	boxy

boxy will automatically load the current openbox menu.


Dependancies
------------

.. image:: http://www.python.org/community/logos/python-logo.png
	:target: http://python.org
	:alt: Python


`lxml <http://lxml.de>`_ - Install
with::	
	pip install lxml


`GTK 2.18+ <http://gtk.org>`_ and `PyGTK. <http://pygtk.org/downloads.html>`_ Install
with::

	pip install PyGTK


OR `Qt <http://qt-project.org>`_ and `PyQT. <http://riverbankcomputing.com/software/pyqt/download>`_ Install
with::
	pip install PyQT










Future
------

Functionality planned but not yet implemented:

- Heuristics or plug in mmaker for premade menus
- Detect GTK/Qt and determine which to use.
- Qt interface and module.
- Merge into an openbox settings manager of the same name.

Credits
-------

- Developed by Luke Tice <luketice@gmail.com>.
- Thanks to all openbox developers and maintainers.
- Special thanks to all testers.

License
-------

.. image:: http://www.gnu.org/graphics/gplv3-127x51.png
	:target: http://www.gnu.org/licenses/gpl.html
	:alt: GPL
