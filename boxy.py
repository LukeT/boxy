""" obtree module usage examples """

from boxy.core.tree import Tree
from boxy.gtk2.window import main
from boxy.core.element import ob

def inspect(e): 
    print ' -------------------\n',e,'-------------------'
    print "desc:",e.ob_desc,"\ntype:",e.ob_type
    print "label:",e.ob_label,"\nid:",e.ob_id
    print "action:",e.ob_action,'\ncommand:',e.ob_command
    print "notify.name:",e.ob_notify,"\nnotify.wmclass:",e.ob_notify_class
    print "to:",e.ob_to,"\nwhere:",e.ob_where
    print "prompt:",e.ob_prompt,"\nicon:",e.ob_icon






main()

